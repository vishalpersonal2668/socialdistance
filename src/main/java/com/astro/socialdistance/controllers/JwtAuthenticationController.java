package com.astro.socialdistance.controllers;

import com.astro.socialdistance.handler.exception.ApiException;
import com.astro.socialdistance.handler.response.ApiResponseCode;
import com.astro.socialdistance.handler.response.ApiResponseDTO;
import com.astro.socialdistance.handler.response.ResponseDTO;
import com.astro.socialdistance.security.JwtRequest;
import com.astro.socialdistance.security.JwtResponse;
import com.astro.socialdistance.business.JwtUserDetailsService;
import com.astro.socialdistance.utils.JwtTokenUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.web.bind.annotation.*;


@RestController
@CrossOrigin
public class JwtAuthenticationController {

    private final JwtUserDetailsService jwtUserDetailsService;
    private final AuthenticationManager authenticationManager;
    private final JwtTokenUtil jwtTokenUtil;

    @Autowired
    public JwtAuthenticationController(JwtUserDetailsService jwtUserDetailsService, AuthenticationManager authenticationManager, JwtTokenUtil jwtTokenUtil) {
        this.jwtUserDetailsService = jwtUserDetailsService;
        this.authenticationManager = authenticationManager;
        this.jwtTokenUtil = jwtTokenUtil;
    }

    @PostMapping(value = "/authenticate")
    public ResponseDTO<?> createAuthenticationToken(@RequestBody JwtRequest authenticationRequest) {
        return new ApiResponseDTO<>(HttpStatus.OK.value(), ApiResponseCode.SUCCESS.getMessage(), new JwtResponse(authenticate(authenticationRequest.getUsername(), authenticationRequest.getPassword())));
    }

    private String authenticate(String username, String password) throws BadCredentialsException {
        try {
            authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(username, password));
        } catch (BadCredentialsException e) {
            throw new ApiException(HttpStatus.BAD_REQUEST.value(), ApiResponseCode.INVALID_USERNAME_PASSWORD.getMessage());
        }

        final UserDetails userDetails = jwtUserDetailsService.loadUserByUsername(username);

        return jwtTokenUtil.generateToken(userDetails);
    }
}
