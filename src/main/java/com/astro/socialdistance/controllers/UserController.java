package com.astro.socialdistance.controllers;

import com.astro.socialdistance.business.UserService;
import com.astro.socialdistance.common.CreateUserRequest;
import com.astro.socialdistance.handler.response.ApiResponseCode;
import com.astro.socialdistance.handler.response.ApiResponseDTO;
import com.astro.socialdistance.handler.response.ResponseDTO;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

@RestController()
@RequestMapping(value = "/api/v1/user")
public class UserController {

    private final UserService userService;

    @Autowired
    public UserController(UserService userService) {
        this.userService = userService;
    }

    @PostMapping(path = "/insert", consumes = "application/json")
    public ResponseDTO<?> createUserProfile(@RequestBody CreateUserRequest userRequest) throws Exception {
        userService.createUserProfile(userRequest);
        return new ApiResponseDTO<>(HttpStatus.OK.value(), ApiResponseCode.SUCCESS.getMessage());
    }

    @GetMapping(path = "/degree")
    public ResponseDTO<?> getKthDegreeConnectionsOfUser(@RequestParam int degree) {
        return new ApiResponseDTO<>(HttpStatus.OK.value(), ApiResponseCode.SUCCESS.getMessage(), userService.getKthDegreeConnections(degree));
    }

    @PostMapping(path = "/add")
    public ResponseDTO<?> addConnectionInFriendsList( @RequestParam String friend) {
        userService.addConnectionInFriendsList(friend);
        return new ApiResponseDTO<>(HttpStatus.OK.value(), ApiResponseCode.SUCCESS.getMessage());
    }

    @DeleteMapping(path = "/delete")
    public ResponseDTO<?> deleteConnectionInFriendList(@RequestParam String friend) {
        userService.deleteConnectionInFriendList(friend);
        return new ApiResponseDTO<>(HttpStatus.OK.value(), ApiResponseCode.SUCCESS.getMessage());
    }

    @GetMapping(path = "/all")
    public ResponseDTO<?> getAllFriendListOfUser(){
        return new ApiResponseDTO<>(HttpStatus.OK.value(), ApiResponseCode.SUCCESS.getMessage(), userService.getAllFriendListOfUser());
    }
}
