package com.astro.socialdistance.constant;

public interface StringConstant {

    String USER = "user=";
    String ERROR = "Sorry Something went Wrong !! We are working on it";

    int JWT_TOKEN_VALIDITY = 3600 * 1000 * 24;
}
