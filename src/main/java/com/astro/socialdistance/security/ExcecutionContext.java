package com.astro.socialdistance.security;

public class ExcecutionContext {

    static ThreadLocal<String> _Username = new ThreadLocal<>();

    public static String get_Username() {
        return _Username.get();
    }

    static void set_Username(String username) {
        ExcecutionContext._Username.set(username);
    }
}
