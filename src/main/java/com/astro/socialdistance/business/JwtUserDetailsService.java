package com.astro.socialdistance.business;

import java.util.ArrayList;
import java.util.Objects;

import com.astro.socialdistance.handler.response.ApiResponseCode;
import com.astro.socialdistance.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

@Service
public class JwtUserDetailsService implements UserDetailsService {

    private final UserRepository userRepository;

    @Autowired
    public JwtUserDetailsService(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @Override
    public UserDetails loadUserByUsername(String username) {

        com.astro.socialdistance.entities.User user = userRepository.findByUsername(username);

        if (Objects.nonNull(user)) {
            return new User(user.getUsername(), user.getPassword(), new ArrayList<>());
        } else {
            throw new UsernameNotFoundException(ApiResponseCode.USER_NOT_FOUND.getMessage() + username);
        }
    }
}
