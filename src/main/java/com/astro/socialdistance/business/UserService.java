package com.astro.socialdistance.business;

import com.astro.socialdistance.common.CreateUserRequest;
import com.astro.socialdistance.entities.Friends;
import com.astro.socialdistance.entities.User;
import com.astro.socialdistance.handler.exception.ApiException;
import com.astro.socialdistance.handler.response.ApiResponseCode;
import com.astro.socialdistance.repository.FriendsRepository;
import com.astro.socialdistance.repository.UserRepository;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.HashSet;
import java.util.List;
import java.util.Objects;

import static com.astro.socialdistance.common.CreateUserRequest.mapUserRequestToUser;
import static com.astro.socialdistance.constant.NumberConstant._0;
import static com.astro.socialdistance.constant.NumberConstant._1;
import static com.astro.socialdistance.security.ExcecutionContext.get_Username;

@Service
public class UserService {

    private final UserRepository userRepository;
    private final FriendsRepository friendsRepository;

    private final BCryptPasswordEncoder bCryptPasswordEncoder;

    private static final Logger log = LogManager.getLogger(UserService.class);

    @Autowired
    public UserService(UserRepository userRepository, FriendsRepository friendsRepository, BCryptPasswordEncoder bCryptPasswordEncoder) {
        this.userRepository = userRepository;
        this.friendsRepository = friendsRepository;
        this.bCryptPasswordEncoder = bCryptPasswordEncoder;
    }

    /**
     * Method is used to create User profile in system
     *
     * @param userRequest CreateUserRequest object contains params for User profile
     * @throws Exception when user already exist
     */
    public void createUserProfile(CreateUserRequest userRequest) throws Exception {
        User alreadyExistedUser = userRepository.findByUsername(userRequest.getUsername());

        log.info("Checking If User Already Exist For - " + userRequest.getUsername());

        if (Objects.nonNull(alreadyExistedUser)) {
            log.error("User Profile Already Exist With - " + userRequest.getUsername());
            throw new ApiException(HttpStatus.BAD_REQUEST.value(), ApiResponseCode.USER_PRESENT.getMessage());
        }

        User user = mapUserRequestToUser(userRequest);
        user.setPassword(bCryptPasswordEncoder.encode(userRequest.getPassword()));
        userRepository.save(user);

        log.info("User Profile Created For - ", userRequest.getUsername());
    }


    /**
     * Method is used to create connection between user and friend
     *
     * @param friend with connection to be made
     * @throws ApiException when friend profile or connection already exist
     */
    public void addConnectionInFriendsList(String friend) throws ApiException {

        if (!friend.equalsIgnoreCase(get_Username())) {

            HashSet<String> userAndFriendSet = new HashSet<String>() {{
                add(get_Username());
                add(friend);
            }};

            List<User> userAndFriendList = userRepository.findUsersByUsername(userAndFriendSet);

            log.info("Checking Both User And Friend Profile");

            if (userAndFriendList.size() == _1) {
                log.info("Friend Profile Doesn't Exist");
                throw new ApiException(HttpStatus.BAD_REQUEST.value(), ApiResponseCode.FRIEND_NOT_PRESENT.getMessage());
            }

            Friends connection = friendsRepository.findByUserAndFriend(userAndFriendList.get(_0).getId(), userAndFriendList.get(_1).getId());

            log.info("Checking If Connection Between User And Friend Already Exist");

            if (Objects.nonNull(connection)) {
                log.info("Connection Between User And Friend Already Exist");
                throw new ApiException(HttpStatus.BAD_REQUEST.value(), ApiResponseCode.CONNECTION_ALREADY_EXIST.getMessage());
            }

            Friends createConnection = new Friends();
            createConnection.setUser(userAndFriendList.get(_0).getId());
            createConnection.setFriend(userAndFriendList.get(_1).getId());
            friendsRepository.save(createConnection);

            log.info("Connection created with friend ", friend);
        } else {
            throw new ApiException(HttpStatus.BAD_REQUEST.value(), ApiResponseCode.CONNECTION_CANNOT_BE_MADE_OR_DELETE_WITH_SAME_USER.getMessage());
        }
    }


    /**
     * Method is used to remove connection between user and friend
     *
     * @param friend with connection to be made
     * @throws ApiException when friend profile or connection already exist
     */
    public void deleteConnectionInFriendList(String friend) throws ApiException {
        if (!friend.equalsIgnoreCase(get_Username())) {

            HashSet<String> userAndFriendSet = new HashSet<String>() {{
                add(get_Username());
                add(friend);
            }};

            List<User> userAndFriendList = userRepository.findUsersByUsername(userAndFriendSet);

            log.info("Checking Both User And Friend Profile");

            if (userAndFriendList.size() == _1) {
                throw new ApiException(HttpStatus.BAD_REQUEST.value(), ApiResponseCode.FRIEND_NOT_PRESENT.getMessage());
            }

            log.info("Checking If Connection Between User And Friend Already Exist");

            Friends connection = friendsRepository.findByUserAndFriend(userAndFriendList.get(_0).getId(), userAndFriendList.get(_1).getId());

            if (Objects.isNull(connection)) {
                log.info("Connection Between User And Friend Already Exist");
                throw new ApiException(HttpStatus.BAD_REQUEST.value(), ApiResponseCode.CONNECTION_NOT_FOUND.getMessage());
            }

            friendsRepository.delete(connection);

            log.info("Connection Removed of friend ", friend);

        } else {
            throw new ApiException(HttpStatus.BAD_REQUEST.value(), ApiResponseCode.CONNECTION_CANNOT_BE_MADE_OR_DELETE_WITH_SAME_USER.getMessage());
        }
    }


    /**
     * Method is used to get all friends of User
     *
     * @return List of profiles of friends
     */
    public List<User> getAllFriendListOfUser() {

        User user = userRepository.findByUsername(get_Username());

        log.info("Finding All Friends Of User");

        @SuppressWarnings("unchecked")
        List<Friends> friendsConnections = friendsRepository.findAllById(user.getId());

        HashSet<Long> friendsList = new HashSet<>();

        /*  Friend Present as User or Friend in Friends Table */
        friendsConnections.forEach((friend) -> {
            if (friend.getUser() == user.getId()) {
                friendsList.add(friend.getFriend());
            } else {
                friendsList.add(friend.getUser());
            }
        });

        return userRepository.findAllFriendsOfUser(friendsList);
    }

    /**
     * Method is used to Get Kth Degree connections.
     *
     * @param degree of connections
     * @return List of friends profile in that degree
     */
    public List<User> getKthDegreeConnections(int degree) {
        if (degree > _0) {
            HashSet<Long> connectionsInAllPreviousDegree = new HashSet<>();

            HashSet<Long> connectionsInCurrentDegree = new HashSet<>();

            User user = userRepository.findByUsername(get_Username());

            /* Connections consists of previous degree */
            connectionsInAllPreviousDegree.add(user.getId());

            /* Connections consists of Current degree */
            connectionsInCurrentDegree.add(user.getId());

            List<Friends> connections;

            while (degree-- > _0) {

                /* If Friends doesn't Exist at that Degree */
                if (connectionsInCurrentDegree.size() == _0) {
                    break;
                }

                connections = friendsRepository.findAllByIds(connectionsInCurrentDegree);

                /* New List for current Degree Friends Only */
                connectionsInCurrentDegree = new HashSet<>();

                /* Adding Connection to Current Connection List which are not Present in All Previous Connection List
                 * And also adding current friends to All previous List also, for removal of friends of lesser degree */
                for (Friends connection : connections) {
                    if (!connectionsInAllPreviousDegree.contains(connection.getFriend())) {
                        connectionsInCurrentDegree.add(connection.getFriend());
                        connectionsInAllPreviousDegree.add(connection.getFriend());
                    } else if (!connectionsInAllPreviousDegree.contains(connection.getUser())) {
                        connectionsInCurrentDegree.add(connection.getUser());
                        connectionsInAllPreviousDegree.add(connection.getUser());
                    }
                }
            }

            return userRepository.findAllFriendsOfUser(connectionsInCurrentDegree);

        } else {
            throw new ApiException(HttpStatus.BAD_REQUEST.value(), ApiResponseCode.INVALID_INPUT.getMessage());
        }
    }
}