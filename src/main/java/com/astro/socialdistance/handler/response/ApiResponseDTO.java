package com.astro.socialdistance.handler.response;

import java.util.HashMap;

import static com.astro.socialdistance.constant.StringConstant.ERROR;

@SuppressWarnings({"unchecked", "rawtypes"})
public class ApiResponseDTO<T> implements ResponseDTO<T>{

    private int code;
    private String message = "";
    private T data;


    public ApiResponseDTO() {
    }

    public ApiResponseDTO(int code, String message, T data) {
        this.code = code;
        this.message = message;
        this.data = data;
    }

    public ApiResponseDTO(int code, String message) {
        this.code = code;
        this.message = message;
        this.data = (T) new HashMap();
    }

    public ResponseDTO exception(int code, String message) {
        return new ApiResponseDTO(code, message, new HashMap());
    }

    public ResponseDTO exception() {
        return new ApiResponseDTO(code, ERROR, new HashMap());
    }

    @Override
    public int getCode() {
        return code;
    }

    @Override
    public void setCode(int code) {
        this.code = code;
    }

    @Override
    public String getMessage() {
        return message;
    }

    @Override
    public T getData() {
        return data;
    }

    @Override
    public void setData(T data) {
        this.data = data;
    }

}
