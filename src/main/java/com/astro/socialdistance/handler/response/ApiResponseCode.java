package com.astro.socialdistance.handler.response;


public enum ApiResponseCode {

    SUCCESS(0, "Success"),
    USER_PRESENT(1, "User Already Exist"),
    FRIEND_NOT_PRESENT(2, "Friend Not Exist"),
    CONNECTION_NOT_FOUND(3, "Connection Not Found"),
    CONNECTION_ALREADY_EXIST(3, "Connection Already Exist"),
    INVALID_USERNAME_PASSWORD(4, "Invalid Username Password"),
    TOKEN_EXPIRED(5, "Token Expired"),
    REQUEST_PAYLOAD_INCORRECT(6, "Request Payload incorrect"),
    USER_NOT_FOUND(7, "User Not Found "),
    INVALID_INPUT(8, "Invalid Input"),
    CONNECTION_CANNOT_BE_MADE_OR_DELETE_WITH_SAME_USER(9, "Connection Cannot Be Made Or Delete With Same User"),
    UNABLE_TO_GET_JWT_TOKEN(10, "Unable To Get Jwt Token");

    private int code;
    private String message;

    ApiResponseCode(int code, String message) {
        this.code = code;
        this.message = message;
    }

    public int getCode() {
        return code;
    }

    public String getMessage() {
        return message;
    }
}
