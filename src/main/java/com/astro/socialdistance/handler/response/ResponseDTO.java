package com.astro.socialdistance.handler.response;


public interface ResponseDTO <T> {

        int getCode();

        void setCode(int code);

        String getMessage();

        T getData();

        void setData(T data);
    }
