package com.astro.socialdistance.handler.exception;

import com.astro.socialdistance.handler.response.ApiResponseDTO;
import com.astro.socialdistance.handler.response.ResponseDTO;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import javax.servlet.http.HttpServletRequest;

@RestControllerAdvice(basePackages = "com.astro.socialdistance")
public class ApiExceptionHandler {

    @ExceptionHandler(value = {ApiException.class})
    public ResponseDTO handleGenericException(ApiException e, HttpServletRequest httpServletRequest) throws Exception {
        return new ApiResponseDTO().exception(e.getCode(), e.getMessage());
    }

    @ExceptionHandler(value = {UsernameNotFoundException.class})
    public ResponseDTO handleGenericException(UsernameNotFoundException e, HttpServletRequest httpServletRequest) throws Exception {
        return new ApiResponseDTO().exception(HttpStatus.NOT_FOUND.value(), e.getMessage());
    }
}
