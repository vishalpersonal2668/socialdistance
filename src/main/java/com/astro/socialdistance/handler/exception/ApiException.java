package com.astro.socialdistance.handler.exception;

public class ApiException extends RuntimeException {

    private int code;
    private String message;

    public ApiException(int code, String error) {
        super(error);
        this.code = code;
        this.message = error;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    @Override
    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

}
