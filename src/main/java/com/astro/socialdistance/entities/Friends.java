package com.astro.socialdistance.entities;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Objects;

@Entity(name = "Friends")
@Table(name = "friends")
public class Friends implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;

    @Column
    private long user;

    @Column
    private long friend;

    public long getUser() {
        return user;
    }

        public void setUser(long user) {
        this.user = user;
    }

    public long getFriend() {
        return friend;
    }

    public void setFriend(long friend) {
        this.friend = friend;
    }

    public Friends() {
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Friends)) return false;
        Friends friends = (Friends) o;
        return getUser() == friends.getUser();
    }

    @Override
    public int hashCode() {

        return Objects.hash(getUser());
    }

    @Override
    public String toString() {
        return "Friends{" +
                "user=" + user +
                ", friend=" + friend +
                '}';
    }
}

