package com.astro.socialdistance.repository;

import com.astro.socialdistance.entities.Friends;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import java.util.List;
import java.util.Set;

public interface FriendsRepository extends CrudRepository<Friends, Long> {

    @Query("SELECT n FROM Friends n WHERE (n.user = ?1 AND n.friend = ?2) OR (n.friend = ?1 AND n.user = ?2)")
    Friends findByUserAndFriend(@Param("user") Long user, @Param("friend") Long friend);

    @Query("SELECT n FROM Friends n WHERE n.user = ?1 OR n.friend = ?1")
    List<Friends> findAllById(@Param("user") Long user);

    @Query("SELECT n FROM Friends n WHERE n.user IN ?1 OR n.friend IN ?1")
    List<Friends> findAllByIds(Set<Long> users);

}
