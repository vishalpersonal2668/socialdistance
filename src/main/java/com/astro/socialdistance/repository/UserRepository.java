package com.astro.socialdistance.repository;

import com.astro.socialdistance.entities.User;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import java.util.List;
import java.util.Set;

public interface UserRepository extends CrudRepository<User, Long>{

    User findByUsername(String username);

    @Query("SELECT n FROM User n WHERE n.id IN ?1")
    List<User> findAllFriendsOfUser(Set<Long> users);

    @Query("SELECT n FROM User n WHERE n.username IN ?1")
    List<User> findUsersByUsername(Set<String> users);

}
