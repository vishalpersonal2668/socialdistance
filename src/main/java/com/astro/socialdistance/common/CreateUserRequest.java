package com.astro.socialdistance.common;

import com.astro.socialdistance.entities.User;
import com.astro.socialdistance.handler.exception.ApiException;
import com.astro.socialdistance.handler.response.ApiResponseCode;
import org.springframework.http.HttpStatus;
import javax.validation.constraints.*;
import java.util.Objects;
import java.util.Random;

public class CreateUserRequest {

    @Pattern(regexp = "^[a-zA-Z]([a-zA-Z0-9 _-])*$", message = "First Name Of Wrong Format")
    private String firstName;

    @NotBlank(message = "Username Name Cannot Be Empty")
    @Pattern(regexp = "^[a-zA-Z]([a-zA-Z0-9 _-])*$", message = "User Name Of Wrong Format")
    private String username;

    @Pattern(regexp = "^[a-zA-Z]([a-zA-Z0-9 _-])*$", message = "Last Name Of Wrong Format")
    private String lastName;

    @Email(message = "Email Of Wrong Format")
    private String email;

    private String password;
    private String address;

    @Pattern(regexp = "^[+]?[() 0-9]{10,}$", message = "Mobile No. Of Wrong Format")
    private long mobile;


    @Pattern(regexp = "^[+]?[() 0-9]{2}$", message = "Age No. Of Wrong Format")
    private int age;

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public long getMobile() {
        return mobile;
    }

    public void setMobile(long mobile) {
        this.mobile = mobile;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public static User mapUserRequestToUser(CreateUserRequest userRequest) throws Exception {
        User user = new User();

        if(Objects.nonNull(userRequest.getUsername()) && !userRequest.getUsername().isEmpty()) {
            user.setUsername(userRequest.getUsername());
        }else {
            throw new ApiException(HttpStatus.BAD_REQUEST.value(), ApiResponseCode.REQUEST_PAYLOAD_INCORRECT.getMessage());
        }

        user.setFirstName(userRequest.getFirstName());
        user.setAddress(userRequest.getAddress());
        user.setLastName(userRequest.getLastName());
        user.setUsername(userRequest.getUsername());
        user.setAge(userRequest.getAge());
        user.setId(new Random().nextLong());
        user.setMobile(userRequest.getMobile());
        return user;
    }
}
