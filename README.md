# README #

Social Distance Project by Vishal Goel

### Technical Stack ###

* Java 1.8+
* Gradle 4.5
* Spring boot 2.1.17.RELEASE
* Jsonwebtoken 0.9.1
* H2 database 1.4.192
* Spring Boot Security and Test

### How To Run ###

* First Create User Profile with all valid Info.
* Use Profile credentials for login.
* After Login, will get JWT token for all other API'S to run.

### Test The Browser ###

* http://3.17.128.12:8080/

### API Go-Through ###

- UserController 
* api/v1/user/insert -  Create Profile of User 
* api/v1/user/degree -  Get All Connections At Degree K 
* api/v1/user/add    -  Add friend in FriendList
* api/v1/user/delete -  Delete friend in FrienList
* api/v1/user/all    -  Get All Friend in FrindList

- JwtAuthenticationController
* /authenticate      -  Login for User

### Repo Modules ###

* Spring Security
* H2 In-Memory Database
* User Service
* Exception Validation
* Api Response

### User Roles ###

* Can Create profile, add or delete friend or get all friends at degree K for only itself. 